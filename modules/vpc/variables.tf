variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
}


/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}

variable "tag_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
  default = "x"
}

