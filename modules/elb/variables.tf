variable "vpc_id" {
  type = string
  description = "vpc id of stack"
}

variable "elb_security_groups" {
  type = list(string)
  description = "ids of security groups to asign at elb"
}

variable "elb_subnets" {
  type = list(string)
  description = "ids of subnets to asign at elb"
}

/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}

variable "tag_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
  default = "x"
}
