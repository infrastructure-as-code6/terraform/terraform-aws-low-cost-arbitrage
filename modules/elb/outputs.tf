output "api_target_group_arn" {
  value = aws_lb_target_group.api_group.arn
  description = "api target group arn"
}

output "front_target_group_arn" {
  value = aws_lb_target_group.front_group.arn
  description = "front target group arn"
}
