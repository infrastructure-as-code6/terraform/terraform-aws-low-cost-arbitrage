terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}


resource "aws_lb" "alb" {
  load_balancer_type = "application"
  ip_address_type = "ipv4"
  name = "ecs-lb-arbitrage"
  internal = false
  security_groups = var.elb_security_groups
  subnets = var.elb_subnets
}

resource "aws_lb_target_group" "api_group" {
  name = "api-target-group"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_api"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/api1/test"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }

}

resource "aws_lb_target_group" "front_group" {
  name = "front-target-group"
  port = 80
  protocol = "HTTP"
  protocol_version = "HTTP1"
  target_type = "instance"
  vpc_id = var.vpc_id
  stickiness {
    cookie_name = "app_cookie_front"
    type = "app_cookie"
    enabled = true
    cookie_duration = 3600
  }
  health_check {
    enabled = true
    interval = 30
    path = "/"
    port = "traffic-port"
    protocol = "HTTP"
    timeout = 2
    healthy_threshold = 2
    unhealthy_threshold = 2
    matcher = "200-299"
  }
}


resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.front_group.arn
  }

}

resource "aws_lb_listener_rule" "listener_rules" {
  listener_arn = aws_lb_listener.alb_listener.arn
  priority = 1
  condition {
    path_pattern {
      values = ["/api1/*"]
    }
  }
  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.api_group.arn

    forward {
      target_group {
        arn = aws_lb_target_group.api_group.arn
        weight = 100
      }
      /*--------beug terraform Solution de contournement :---------
      Je met 2 target-groups identiques sinon terraform refuse de compiler.*/
      target_group {
        arn = aws_lb_target_group.api_group.arn
      }
      /*---------------------------------------------------------*/
      stickiness {
        duration = 3600
        enabled = true
      }
    }
  }
}
