variable "log_group_name" {
  type = string
  description = "name of log group"
}

variable "container_info_api" {
  type = object({
    name = string
    port = number
    image = string
    env = list(object({
      name = string
      value = string
    }))
  })
  description = "Object with api container infos"
}

variable "container_info_front" {
  type = object({
    name = string
    port = number
    image = string
    env = list(object({
      name = string
      value = string
    }))
  })
  description = "Object with front container infos"
}

variable "target_group_arns" {
  type = object({
    api = string
    front = string
  })
  description = "arn of each target groups used by alb"
}

variable "cluster_id" {
  type = string
  description = "id of ecs cluster"
}


/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}

variable "tag_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
  default = "x"
}
