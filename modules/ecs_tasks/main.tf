terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}

data "aws_region" "current"{}
data "aws_caller_identity" "current" {}

resource "aws_ecs_task_definition" "task_api" {
  family = "api-filtrage"
  execution_role_arn = aws_iam_role.ecs_task_execution.arn
  network_mode = "bridge"
  cpu = 300
  memory = 300
  requires_compatibilities = ["EC2"]
  container_definitions = jsonencode([
    {
      name      = var.container_info_api.name
      image     = var.container_info_api.image
      command = ["npm", "run", "start"]
      essential = true
      workingDirectory = "/usr/src/app"
      essential = true
      environment = var.container_info_api.env
      portMappings = [{
          containerPort = var.container_info_api.port
          hostPort      = 0
      }]
      healthCheck = {
        retries: 3
        command = ["CMD-SHELL","curl -f http://127.0.0.1:${var.container_info_api.port}/api1/test || exit 1"]
        timeout = 15
        interval = 45
        startPeriod = 10
      }
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = var.log_group_name
          awslogs-region = data.aws_region.current.name
          awslogs-stream-prefix = "api"
        }
      }
    },
  ])
}

resource "aws_ecs_task_definition" "task_front" {
  family = "front-adminPanel"
  execution_role_arn = aws_iam_role.ecs_task_execution.arn
  network_mode = "bridge"
  cpu = 250
  memory = 280
  requires_compatibilities = ["EC2"]
  container_definitions = jsonencode([
    {
      name      = var.container_info_front.name
      image     = var.container_info_front.image
      command = [
        "/bin/bash",
        "-c",
        "envsubst '$SITE_HOSTNAME $SITE_PORT'< /etc/nginx/conf.d/server_web.conf.template > /etc/nginx/conf.d/server_web.conf && nginx -g 'daemon off;'"
      ]
      workingDirectory = "/etc/nginx/conf.d"
      cpu = 250
      memory = 280
      memoryReservation = 190
      essential = true
      essential = true
      environment = var.container_info_front.env
      portMappings = [{
          containerPort = var.container_info_front.port
          hostPort      = 0
      }]
      healthCheck = {
        retries: 3
        command = ["CMD-SHELL","curl -f http://127.0.0.1:${var.container_info_front.port} || exit 1"]
        timeout = 3
        interval = 30
        startPeriod = 15
      }
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group = var.log_group_name
          awslogs-region = data.aws_region.current.name
          awslogs-stream-prefix = "front"
        }
      }
    },
  ])
}

resource "aws_ecs_service" "service_api" {
  name = "api-service"
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.task_api.arn
  desired_count = 1
  scheduling_strategy = "REPLICA"
  launch_type = "EC2"
  deployment_maximum_percent = 200
  deployment_minimum_healthy_percent = 100
  health_check_grace_period_seconds = 30
  ordered_placement_strategy {
    type = "binpack"
    field = "memory"
  }
  deployment_circuit_breaker {
    enable   = true
    rollback = false
  }
  deployment_controller {
    type = "ECS"
  }
  load_balancer {
    container_name = var.container_info_api.name
    container_port = var.container_info_api.port
    target_group_arn = var.target_group_arns.api
  }
  iam_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
}

resource "aws_ecs_service" "service_front" {
  name = "front-service"
  cluster = var.cluster_id
  task_definition = aws_ecs_task_definition.task_front.arn
  desired_count = 1
  scheduling_strategy = "REPLICA"
  launch_type = "EC2"
  deployment_maximum_percent = 200
  deployment_minimum_healthy_percent = 100
  health_check_grace_period_seconds = 30
  ordered_placement_strategy {
    type = "binpack"
    field = "memory"
  }
  deployment_circuit_breaker {
    enable   = true
    rollback = false
  }
  deployment_controller {
    type = "ECS"
  }
  load_balancer {
    container_name = var.container_info_front.name
    container_port = var.container_info_front.port
    target_group_arn = var.target_group_arns.front
  }
  iam_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
}

/*----------IAM Roles ----------------*/
resource "aws_iam_role" "ecs_task_execution" {
  name = "role-ecs-task-execution"
  description = "Provides access to other AWS service resources that are required to run Amazon ECS tasks"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect : "Allow"
      Action = "sts:AssumeRole"
      Principal = {
        Service = "ecs-tasks.amazonaws.com"
      }
    }]
  })
}

