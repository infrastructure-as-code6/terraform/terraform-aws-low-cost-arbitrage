
variable "vpc_id" {
  type = string
  description = "vpc id of stack"
}

variable "ingress_rules_cidr" {
  type = list(object({
    protocol = string
    from_port = number
    to_port = number
    cidr_blocks = list(string)
  }))
  default = []
}

variable "ingress_rules_sg" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
    security_groups = list(string)
  }))
  default = []
}

variable "sg_name" {
  type = string
  description = "security group name"
}

/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}

variable "tag_name_suffix" {
  type = string
  description = "suffix for 'Name' tag"
  default = "x"
}


