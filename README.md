- **Only if this repository doesn't use `Vault HashiCorp`**

Then create file named `_secrets.auto.tfvars` for asign sensitives variables stored in `secret.tf` file'
```bash
touch _secrets.auto.tfvars
```

`_secrets.auto.tfvars` :
```terraform
secret1 = "secret-of-the-world"
secret2 = "hello-world"
...
```

- **Else do nothing :blush:** 

All secrets are already got in `secret.tf` thanks to `Vault HashiCorp` provider
