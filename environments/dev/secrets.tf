variable "my_ip" {
  type = string
  description = "my public ip address for allow ssh connexions"
  sensitive = true
}

variable "key_pair_name" {
  type = string
  description = "Name of aws '.pem' key pair for ec2 instances for ssh authentification"
  sensitive = true
}

locals {
  secrets = {
    my_ip = var.my_ip # my public ip address for allow ssh connexions
    key_pair_name = var.key_pair_name # Name of '.pem' key pair for ec2 instances for ssh authentification
  }
}
