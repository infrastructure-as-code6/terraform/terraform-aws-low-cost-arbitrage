variable "environment" {
  type = string
  description = "envrionement name"
}
variable "project" {
  type = string
  description = "project name"
}

variable "resources_tags" {
  type = map(string)
  description = "resources default tags"
  default = {}
}

variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
}

variable "log_group_name" {
  type = string
  description = "name of cloudwatch log group"
}

variable "ami_id" {
  type = string
  description = "ami id for ecs instances"
}


variable "container_info_api" {
  type = object({
    name = string
    image = string
    port = number
  })
  description = "Api container informations"
}

variable "container_info_front" {
  type = object({
    name = string
    image = string
    port = number
  })
  description = "Front container informations"
}


