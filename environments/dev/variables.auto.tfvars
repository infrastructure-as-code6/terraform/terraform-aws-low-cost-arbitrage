project = "free-abt-terraform"

vpc_cidr_block = "10.0.0.0/16"

environment = "development"

log_group_name = "arbitrage"

ami_id = "ami-00f030d3d7a345389"

container_info_api = {
  name = "front-container"
  image = "enzo13/api-arbitrage-p:3.0.1"
  port = 3000
}

container_info_front = {
  name = "front-container"
  image = "enzo13/front-arbitrage-p:3.0"
  port = 4200
}

resources_tags = {}

