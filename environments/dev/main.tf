locals {
  tag_name_suffix = "${var.project}-${var.environment}"
  required_tags = {
    Project = var.project
    Environment = var.environment
  }
  tags = merge(var.resources_tags, local.required_tags)
}


provider "aws" {
  region = "us-east-2"
  profile = "terraform"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-5hr6tshr6t8h4"
    key    = "terraform/dev/terraform.tfstate"
    region = "us-east-2"
    encrypt = true
  }
}

module "vpc" {
  source = "./../../modules/vpc"
  vpc_cidr_block = var.vpc_cidr_block
  tag_name_suffix = local.tag_name_suffix
  resources_tags = local.tags
}

module "sg_elb" {
  source = "../../modules/security_group"
  vpc_id = module.vpc.vpc_id
  sg_name = "public-elb-sg"
  ingress_rules_cidr = [{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [local.secrets.my_ip]
  },{
    from_port = 80
    to_port = 80
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }]
  tag_name_suffix = local.tag_name_suffix
  resources_tags = local.tags
}

module "sg_ec2" {
  source = "../../modules/security_group"
  vpc_id = module.vpc.vpc_id
  sg_name = "restricted-ec2-sg"
  ingress_rules_cidr = [{
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  },{
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [var.vpc_cidr_block]
  }]
  tag_name_suffix = local.tag_name_suffix
  resources_tags = local.tags
}

module "elb" {
  source = "../../modules/elb"
  vpc_id = module.vpc.vpc_id
  elb_security_groups = [module.sg_elb.sg_id]
  elb_subnets = module.vpc.public_subnet_ids
  tag_name_suffix = local.tag_name_suffix
  resources_tags = local.tags
}

module "ecs_cluster"{
  source = "../../modules/ecs_cluster"
  cluster_name = "ecs-arbitrage-cluster-dev"
  vpc_id = module.vpc.vpc_id
  subnets_ids = module.vpc.public_subnet_ids
  tag_name_suffix = local.tag_name_suffix
  resources_tags = local.tags
  ami_id = var.ami_id
  instance_type = "t2.micro"
  key_name = local.secrets.key_pair_name
  sg_ids = [module.sg_ec2.sg_id]
}


resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
  name = var.log_group_name
  retention_in_days = 5
}


module "ecs_services"{
  source = "../../modules/ecs_tasks"
  cluster_id = module.ecs_cluster.cluster_id
  container_info_api = {
    port = var.container_info_api.port
    name = "api-container"
    env = [
      { name: "API_NAME", value: "api1" },
      { name: "API_PORT", value: var.container_info_api.port },
      { name: "MONGO_DB", value: null },
      { name: "MONGO_HOSTNAME", value: "mongo.enzo-cora.com"},
      { name: "MONGO_INITDB_USERNAME", value: "api"},
      { name: "MONGO_INITDB_PASSWORD", value: "apipwd"},
      { name: "MONGO_PORT", value: 27017},
      { name: "NODE_ENV", value: "production"}
    ]
    image = var.container_info_api.image
  }
  container_info_front = {
    port = var.container_info_front.port
    name = "front-container"
    image = var.container_info_front.image
    env = [
      { name: "SITE_HOSTNAME", value: "front-arbitrage-p"},
      { name: "SITE_PORT", value: var.container_info_front.port}
    ]
  }
  log_group_name = aws_cloudwatch_log_group.cloudwatch_log_group.name
  target_group_arns = {
    api = module.elb.api_target_group_arn
    front = module.elb.front_target_group_arn
  }
}
